var mongoose = require('moongose');
var User = mongoose.model('User');
module.export.register = function(req,res) {
    console.log('registering user');

    var username = req.body.username;
    var name = req.body.name || null;
    var password = req.body.password;

    User.create({
        username: username,
        name: name,
        password: password
    }, function(err, user) {
        if(err) {
            console.log(err);
            res.status(400).json(err);
        } else {
            console.log('User created', user);
            res.status(201).json(user);
        }
    });
};

module.export.login = function(req,res) {
    console.log('logging in user');
    var username = req.body.username;
    var password = req.body.password;

    User.findOne({
        username: username
    }).exec (function(err, user) {
        if(err) {
            console.log(err);
            res.status(400).json(err);
        }else{
            console.log('User found', user);
            res.status(200).json(user);
        }
        });

};
