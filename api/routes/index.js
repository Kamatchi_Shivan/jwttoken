var express = require('express');
var router = express.Router();

var ctrlHotels = require('../controller/hotels.controllers.js');
var ctrlReviews = require('../controller/reviews.controllers.js');

router
.route('/hotels')
.get(ctrlHotels.hotelsGetAll)
.post(ctrlHotels.hotelsAddOne);

router
.route('/hotels/:hotelId')
.get(ctrlHotels.hotelsGetOne)
.put(ctrlHotels.hotelsUpdateOne);

router
.router('/hotels/:hotelsId/reviews/:reviewId')
.get(ctrlReviews.reviewsGetOne)
.put(ctrlReviews.reviewsUpdateOne);

router
.route('/user/register')
.post(ctrlUsers.register);

router
.route('/user/login')
.post(ctrlUsers.login)
module.exports = router;

